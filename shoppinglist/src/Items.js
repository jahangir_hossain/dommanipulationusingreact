import React from 'react';

const Items = ({items, deleteItem}) => {
    const ItemList = items.length ? (
        items.map( item => {
            return(
                <li className="list-group-item list-group-item-primary" key = {item.id}>
                                <div className="row justify-content-between m-0">
                                    <div className="col-4">
                                            {item.name}
                                    </div>
                                    <div className="col-auto right">
                                            <button type="button" className="btn btn-sm btn-danger " onClick={ () => {deleteItem(item.id)}}>Delete</button>
                                    </div>
                                </div>
                            </li>
            )
        })
    ) : (
        <p className="center">your shopping list is empty!</p>
    );

    return(
        <div className="Container">
            {ItemList}
        </div>
    )
}



export default Items;