import React, {Component} from 'react'

class AddItem extends Component{

    state = {
        name: ''
    }

    handleChange = (e) => {
        console.log(this.props);
        this.setState({
            name: e.target.value
        })
    }

    handleSubmit = (e) =>{
        e.preventDefault();
        this.props.addItem(this.state);
        console.log(this.state);
        this.setState({
            name:''
        })
    }

    render(){
        return(
            <div>
            <form onSubmit={this.handleSubmit}>
              <label>Add a new Item:</label>
              <input type="text" onChange={this.handleChange} value={this.state.name} />
            </form>
          </div>
            
        )
    }

} 

export default AddItem;