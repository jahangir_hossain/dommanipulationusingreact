import React, { Component } from 'react';
import Items from './Items'
import AddItem from './AddItem'

class App extends Component {
  state = {
    items:[
      {id: 1, name: "Apple"},
      {id: 2, name: "Mango"}
  ]
  }

  addItem = (item) => {
    item.id = Math.random()*10;
    this.setState({
      items: [...this.state.items, item]
    }
      
    )

  }

  deleteItem = (id) => {
    let items = this.state.items.filter( item => {return item.id !== id})
    this.setState({
      items
    }
      
    )
  } 



  render() {
    return (
      <div className="App">
          <div className="container">
            <div className="row justify-content-center">
                <div className="col-6">
                        <h1 className="text-center text-danger">Shopping List</h1>
                        <Items items = {this.state.items} deleteItem = {this.deleteItem}/>
                </div>
            </div>

            <div className="row justify-content-center">
                <div className="col-6">
                      <AddItem addItem = {this.addItem}/>  
                </div>
            </div>
            </div>
        
      </div>
    );
  }
}

export default App;
